-module(ct_data_util_test).
-include_lib("eunit/include/eunit.hrl").

-define(MNESIADIR, "db/mnesia").

start_mnesia_test() ->
    ok = ct_data_util:stop_mnesia(?MNESIADIR).

stop_mnesia_test() ->
    ok = ct_data_util:stop_mnesia(?MNESIADIR).

create_mnesia_schema_test() ->
    ok = ct_data_util:create_mnesia_schema_if_needed(?MNESIADIR),
    ok = ct_data_util:create_mnesia_schema_if_needed(?MNESIADIR),
    ok.

