-module(ct_data_util).

-export([
         stop_mnesia/1,
         start_mnesia/0,
         create_mnesia_schema_if_needed/1
        ]).


-spec stop_mnesia(binary() | list()) -> ok.
stop_mnesia(BinDir) when is_binary(BinDir) ->
    MnesiaDir = binary_to_list(BinDir),
    stop_mnesia(MnesiaDir);
stop_mnesia(MnesiaDir) when is_list(MnesiaDir) ->
    application:stop(mnesia),
    application:set_env(mnesia, dir, MnesiaDir),
    ok.

-spec start_mnesia() -> ok.
start_mnesia() ->
    {ok, _} = application:ensure_all_started(mnesia),
    ok.


-spec create_mnesia_schema_if_needed(binary()) -> ok.
create_mnesia_schema_if_needed(MnesiaDir) ->
    ok = filelib:ensure_dir(MnesiaDir),
    DirExists = filelib:is_dir(MnesiaDir),
    create_mnesia_schema_if_needed(DirExists, MnesiaDir).


-spec create_mnesia_schema_if_needed(In, Path) -> Result
  when
    In :: boolean(),
    Path :: binary(),
    Result :: ok.
create_mnesia_schema_if_needed(true, _MnesiaDir) ->
    ok;
create_mnesia_schema_if_needed(false, MnesiaDir) ->
    ok = stop_mnesia(MnesiaDir),
    ok = mnesia:create_schema([node()]),
    ok = start_mnesia(),
    ok.


